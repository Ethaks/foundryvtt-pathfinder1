_id: efwHEwjvL9Z94MJI
_key: '!items!efwHEwjvL9Z94MJI'
_stats:
  coreVersion: '12.331'
folder: RWSoA7Fj9yqo0y9U
img: systems/pf1/icons/races/gnome.png
name: Gnome
system:
  changes:
    - _id: kezkn3k4
      formula: '2'
      target: con
      type: racial
    - _id: 1yu39n9h
      formula: '2'
      target: cha
      type: racial
    - _id: pi8lg4y1
      formula: '-2'
      target: str
      type: racial
    - _id: cssv57sq
      formula: '2'
      target: skill.per
      type: racial
  contextNotes:
    - target: ac
      text: +[[4]] Dodge vs Giants
    - target: allSavingThrows
      text: +[[2]] Racial vs Illusion Effects
    - target: attack
      text: +[[1]] Racial vs Humanoids (Reptillian, Goblinoid)
  creatureSubtypes:
    - gnome
  creatureTypes:
    - humanoid
  description:
    value: >-
      <h2>Starting
      Age</h2><table><thead><tr><th>Adulthood</th><th>Intuitive<sup>1</sup></th><th>Self-Taught<sup>2</sup></th><th>Trained<sup>3</sup></th></tr></thead><tbody><tr><td>40
      years</td><td><p>+4d6 years<br />(44 – 64 years)</p></td><td><p>+6d6
      years<br />(46 – 76 years)</p></td><td><p>+9d6 years<br />(49 – 94
      years)</p></td></tr></tbody></table><p><sup>1</sup> This category includes
      barbarians, oracles, rogues, and sorcerers.<br /><sup>2</sup> This
      category includes bards, cavaliers, fighters, gunslingers, paladins,
      rangers, summoners, and witches.<br /><sup>3</sup> This category includes
      alchemists, clerics, druids, inquisitors, magi, monks, and
      wizards.</p><h2>Height and
      Weight</h2><table><thead><tr><th>Gender</th><th>Base Height</th><th>Height
      Modifier</th><th>Base Weight</th><th>Weight
      Modifier</th></tr></thead><tbody><tr><td>Male</td><td>3 ft. 0
      in.</td><td><p>+2d4 in.<br />(3 ft. 2 in. – 3 ft. 8 in.)</p></td><td>35
      lbs.</td><td><p>+(2d4 lbs.)<br />(37 – 43
      lbs.)</p></td></tr><tr><td>Female</td><td>2 ft. 10 in.</td><td><p>+2d4
      in.<br />(3 ft. 0 in. – 3 ft. 6 in.)</p></td><td>30 lbs.</td><td><p>+(2d4
      lbs.)<br />(32 – 38 lbs.)</p></td></tr></tbody></table><h2>Standard Racial
      Traits</h2><ul><li><strong>Ability Score Modifiers</strong>: Gnomes are
      physically weak but surprisingly hardy, and their attitude makes them
      naturally agreeable. They gain +2 <em>Constitution</em>, +2
      <em>Charisma</em>, and -2
      <em>Strength</em>.</li><li><strong>Type</strong>: Gnomes are Humanoid
      creatures with the gnome subtype.</li><li><strong>Size</strong>: Gnomes
      are Small creatures and thus gain a +1 <em>size bonus</em> to their AC, a
      +1 <em>size bonus</em> on <em>attack rolls</em>, a -1 penalty to their
      <em>Combat Maneuver Bonus</em> and <em>Combat Maneuver Defense</em>, and a
      +4 size bonus on <em>Stealth</em> checks.</li><li><strong>Base Speed
      (Slow)</strong>: Gnomes have a base speed of 20
      feet.</li><li><strong>Languages</strong>: Gnomes begin play speaking
      Common, Gnome, and Sylvan. Gnomes with high <em>Intelligence</em> scores
      can choose from the following: Draconic, Dwarven, Elven, Giant, Goblin,
      and Orc. See the <em>Linguistics skill page</em> for more information
      about these languages.</li></ul><h3>Defense Racial
      Traits</h3><ul><li><strong>Defensive Training</strong>: Gnomes gain a +4
      <em>dodge bonus</em> to AC against monsters of the <em>giant</em>
      subtype.</li><li><strong>Illusion Resistance</strong>: Gnomes gain a +2
      racial saving throw bonus against <em>illusion</em> spells and
      effects.</li></ul><h3>Feat and Skill Racial
      Traits</h3><ul><li><strong>Keen Senses</strong>: Gnomes receive a +2
      <em>racial bonus</em> on
      @UUID[Compendium.pf1.pf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.2h6hz5AkTDxKPFxr]{Perception}
      checks.</li><li><strong>Obsessive</strong>: Gnomes receive a +2 <em>racial
      bonus</em> on a
      @UUID[Compendium.pf1.pf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.3E8pxbjI8MD3JbfQ]{Craft}
      or
      @UUID[Compendium.pf1.pf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.224EaK0K72NhCeRi]{Profession}
      skill of their choice.</li></ul><h3>Magical Racial
      Traits</h3><ul><li><strong>Gnome Magic</strong>: Gnomes add +1 to the DC
      of any saving throws against <em>illusion</em> spells that they cast.
      Gnomes with <em>Charisma</em> scores of 11 or higher also gain the
      following <em>spell-like abilities</em>: 1/day —
      @UUID[Compendium.pf1.spells.zymaptg3vmnvfvxl]{Dancing Lights},
      @UUID[Compendium.pf1.spells.mtxqp85izkb20djq]{Ghost Sound},
      @UUID[Compendium.pf1.spells.a6isom7wx0nmhwfp]{Prestidigitation}, and
      @UUID[Compendium.pf1.spells.ct9fhh8uawn8e4md]{Speak with Animals}. The
      <em>caster level</em> for these effects is equal to the gnome's level. The
      DC for these spells is equal to 10 + the spell's level + the gnome's
      <em>Charisma</em> modifier.</li></ul><h3>Offensive Racial
      Traits</h3><ul><li><strong>Hatred</strong>: Gnomes receive a +1 bonus on
      <em>attack rolls</em> against <em>humanoid</em> creatures of the
      <em>reptilian</em> and <em>goblinoid</em> subtypes because of their
      special training against these hated foes.</li><li><strong>Weapon
      Familiarity</strong>: Gnomes treat any weapon with the word "gnome" in its
      name as a martial weapon.</li></ul><h3>Senses Racial
      Traits</h3><ul><li><strong>Low-Light Vision</strong>: Gnomes can see twice
      as far as <em>humans</em> in conditions of dim light.</li></ul>
  languages:
    - common
    - gnome
    - sylvan
  size: sm
  sources:
    - id: PZO1110
      pages: '23'
  speeds:
    land: 20
  tags:
    - Core
type: race
