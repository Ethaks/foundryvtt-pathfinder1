_id: QKrRHanuWwD4pu8Q
_key: '!items!QKrRHanuWwD4pu8Q'
_stats:
  coreVersion: '12.331'
folder: CWHsfTdEmZohUkw1
img: icons/creatures/abilities/tail-swipe-green.webp
name: Ganzi
system:
  changes:
    - _id: lndm8a6h
      formula: '2'
      target: con
      type: racial
    - _id: 0upeyqnv
      formula: '2'
      target: cha
      type: racial
    - _id: 5e3pgybm
      formula: '-2'
      target: int
      type: racial
    - _id: fd8wyhkr
      formula: '2'
      target: skill.sen
      type: racial
    - _id: ozblcz4r
      formula: '2'
      target: skill.sur
      type: racial
  creatureSubtypes:
    - native
  creatureTypes:
    - outsider
  description:
    value: >-
      <h2>Standard Racial Traits</h2><ul><li><strong>Ability Score
      Modifiers</strong>: Ganzis are passionate, creative, and resilient, but
      they have difficulty focusing on intellectual pursuits. They gain +2
      <em>Constitution</em> and <em>Charisma</em>, but suffer –2 to
      <em>Intelligence</em>.</li><li><strong>Type</strong>: Ganzi are
      <em>outsiders</em> with the <em>native</em>
      subtype.</li><li><strong>Size</strong>: Ganzis are Medium creatures and
      have no bonuses of penalties due to their size.</li><li><strong>Base
      Speed</strong>: Ganzis have a base speed of 30
      feet.</li><li><strong>Languages</strong>: Ganzis begin play speaking
      Common and one of the following: Abyssal, Celestial, or Protean. Ganzis
      with high <em>Intelligence</em> scores can select any of those languages
      as bonus languages, along with Aklo, Aquan, Auran, Ignan, Sylvan, or
      Terran.</li></ul><h3>Defense Racial Traits</h3><ul><li><strong>Maelstrom
      Resistance</strong>: Ganzis have <em>acid resistance</em> 5,
      <em>electricity resistance</em> 5, and <em>sonic resistance</em> 5, and
      they gain a +2 <em>racial bonus</em> on saving throws against spells of
      the <em>polymorph</em> subschool.</li></ul><h3>Feat and Skill Racial
      Traits</h3><ul><li><strong>Skilled</strong>: Ganzis gain a +2 <em>racial
      bonus</em> on <em>Sense Motive</em> and <em>Survival</em>
      checks.</li></ul><h3>Senses Racial
      Traits</h3><ul><li><strong>Darkvision</strong>: Ganzis have
      <em>darkvision</em> 60 feet.</li></ul><h3>Other Racial
      Traits</h3><ul><li><p><strong>Ganzi Oddity</strong>: As creatures infused
      with the entropic forces of the Maelstrom, all ganzis have a distinct and
      unusual oddity that manifests as a physical or magical ability that sets
      the ganzi apart. Typically, a ganzi’s oddity is chosen from the most
      common manifestations of entropy listed below, but other, more unique
      oddities can certainly exist. The numbers listed for each option can be
      used to randomly determine what form of ganzi oddity a particular Ganzi
      might have, at the GM’s option, but the quibble ability is the most
      common.</p><ul><li><strong>1–2: Chaos Champion (Su)</strong> Ganzis with a
      lineage linked to the einherji often become crusaders for the forces of
      entropy and excel in combat against lawful beings. The ganzi can smite law
      once per day as a <em>swift action</em>, targeting a lawful foe. She adds
      her <em>Charisma</em> modifier as a bonus on <em>attack rolls</em> and
      gains a bonus on damage rolls equal to her <em>Hit Dice</em>. Once
      activated, smite law persists until the target is dead or the ganzi rests,
      whichever comes first.</li><li><strong>3–4: Entropic Flesh (Ex)</strong>
      Ganzis with lineages that link strongly to <em>chaos beasts</em> retain
      some of their ancestors' <em>immunity</em> to transformation. In addition
      to gaining a +2 bonus on <em>Escape Artist</em> checks and to <em>CMD</em>
      against <em>grapple combat maneuvers</em>, when such ganzis are affected
      by a hostile transmutation effect (such as polymorphing or petrification),
      they can attempt a new saving throw at the start of their next turn to end
      the effect. They gain a +4 bonus on this second saving throw, but if they
      fail, the <em>transmutation</em> effect persists as long as it normally
      would.</li><li><strong>5–6: Prehensile Tail (Ex)</strong> Ganzis with
      lineages that are particularly influenced by the presence of
      <em>proteans</em> often have colorful feathers, scales, or skin, and a
      long serpentine tail. This tail can be used to carry items. While ganzis
      with this oddity cannot wield weapons with their tails, they can use their
      tails to retrieve small, stowed objects carried on their persons as a
      <em>swift action</em>.</li><li><strong>7–14: Quibble (Su)</strong> For
      most ganzis, the influence of chaos is relatively subtle and manifests as
      an ability to infect others with entropy and unexpected luck (be it good
      luck or bad). Once per day as an <em>immediate action</em>, a ganzi can
      twist probability and alter the luck of a single creature within 20 feet,
      forcing it to reroll a single d20 roll it has just made. The target must
      take the second result, even if it is worse. A ganzi can use this ability
      after the target has rolled but must use it before the GM declares whether
      the roll was a success or failure. Unwilling creatures can resist a
      ganzi’s quibble with a successful <em>Will</em> save (DC = 10 + half the
      ganzi's level + the ganzi's <em>Charisma</em> modifier). This is a curse
      effect.</li><li><strong>15–18: Spell-Like Ability (Sp)</strong> Ganzis
      whose chaotic influences rise from regions other than the Maelstrom or its
      inhabitants often manifest an oddity that duplicates the effects of a
      specific spell. Such ganzis gain one of the following <em>spell-like</em>
      abilities, usable once per day at a <em>caster level</em> equal to the
      ganzi's HD: @UUID[Compendium.pf1.spells.kouqz0pm1xl8xilm]{alter self},
      @UUID[Compendium.pf1.spells.18kryvj2dfuymk9d]{blur},
      @UUID[Compendium.pf1.spells.xllxylvvqr82o2d5]{detect thoughts},
      @UUID[Compendium.pf1.spells.5nr9o7o0it6ewf17]{hideous laughter},
      @UUID[Compendium.pf1.spells.27o3msobhyghmfid]{minor image},
      @UUID[Compendium.pf1.spells.q63jn0kbrrb6t3ea]{shatter}, or
      @UUID[Compendium.pf1.spells.3mfmhx8avu7h3iom]{spider
      climb}.</li><li><strong>19–20: Weaponplay (Ex) </strong>Ganzis with
      lineages that link to the valkyries manifest their heritage in their skill
      in combat. Such ganzis are always proficient with all simple and martial
      weapons, and they can qualify for feats normally available only to
      <em>fighters</em> (such as <em>Weapon Specialization</em>) even if they
      don't have any <em>fighter</em> class levels.</li></ul></li></ul>
  languages:
    - common
  sources:
    - id: PZO1141
      pages: '234'
  speeds:
    land: 30
  tags:
    - Planetouched
type: race
