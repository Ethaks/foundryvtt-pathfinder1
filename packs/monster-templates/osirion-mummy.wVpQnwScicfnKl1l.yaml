_id: wVpQnwScicfnKl1l
_key: '!items!wVpQnwScicfnKl1l'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Osirion Mummy
system:
  changes:
    - _id: 1rorl0sn
      formula: '5'
      target: nac
      type: untyped
    - _id: 8e9br02f
      formula: '-10'
      target: allSpeeds
      type: base
    - _id: pb56y66m
      formula: '4'
      target: str
      type: untyped
    - _id: wmnyx188
      formula: '-2'
      target: int
      type: untyped
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>The mummification process of ancient
      Osirion results in a variant mummy. Although the Osirion mummy appears
      very similar to normal mummies-a desiccated husklike creature, draped in
      embalming wrap adorned with hieroglyphics-the Osirion mummy differs
      slightly in ability. Osirion mummies do not spread the curse of mummy rot
      through touch, nor does the very sight of them give rise to paralysis.
      They are still, however, resilient killing machines.<p>"Osirion mummy" is
      an acquired template that can be added to any living, corporeal creature
      (hereafter referred to as the base creature). An Osirion mummy uses all of
      the base creature's statistics except as noted here.<p><b>Size and
      Type:</b> The creature's type changes to undead, and it gains the
      augmented subtype. It retains any other subtypes as well, except for
      alignment subtypes (such as good). Do not recalculate base attack bonuses,
      saves, or skill points. Size is unchanged.<p><b>Hit Dice:</b> Increase all
      current and future Hit Dice to d12.<p><b>AC:</b> An Osirion mummy's
      natural armor bonus increases by +5.<p><b>Defensive Abilities:</b> An
      Osirion mummy retains the base creature's defensive abilities and gains
      damage reduction.<p><em>Damage Reduction (Ex) </em>An Osirion mummy's body
      is resilient, providing it with damage reduction 5/-.<p><b>Weaknesses:</b>
      An Osirion mummy retains the base creature's weaknesses and gains energy
      vulnerability.<p><em>Energy vulnerability (Ex)</em> The mummification
      process leaves the mummy vulnerable to a single energy type, from which it
      takes half again as much damage (+50%) as normal. Choose or determine
      randomly from the following list:</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>d10</b></td>

      <td><b>Energy</b></td>

      </tr>

      <tr>

      <td>1-4</td>

      <td>Fire</td>

      </tr>

      <tr>

      <td>5-6</td>

      <td>Acid</td>

      </tr>

      <tr>

      <td>7-8</td>

      <td>Cold</td>

      </tr>

      <tr>

      <td>9</td>

      <td>Electricity</td>

      </tr>

      <tr>

      <td>10</td>

      <td>Sonic</td>

      </tr>

      </tbody>

      </table>

      <p>As an emergency safeguard, it was common for the necromantic embalmers
      of ancient Osirion to subtly mark the particular energy type to which the
      mummy would be vulnerable with a separate hieroglyph someplace
      inconspicuously on the mummy's body or wrappings. A DC 20 Spot check
      uncovers the mark, but unless the viewer is capable of comprehending its
      meaning a DC 20 Decipher Script check is required to unlock its
      secret.<p><b>Speed:</b> An Osirion mummy's speeds all decrease by 10 feet
      (minimum 5 feet). If the base creature has a flight speed its
      maneuverability class worsenes by one step, to a minimum of
      clumsy.<p><b>Attack:</b> An Osirion mummy retains all the attacks of the
      base creature and also gains a slam attack if it did not already have one.
      If the base creature can use weapons, the Osirion mummy retains that
      ability. In addition, all of an Osirion mummy's attacks are treated as
      magical for the purpose of overcoming damage reduction.<p><b>Damage:</b>
      The mummification process hardens the mummy's bones to a stonelike
      density, granting the mummy a powerful slam attack. The creature's slam
      attack deals damage according to its size as listed below.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Size</b></td>

      <td><b>Damage</b></td>

      </tr>

      <tr>

      <td>Fine</td>

      <td>1</td>

      </tr>

      <tr>

      <td>Diminutive</td>

      <td>1d2</td>

      </tr>

      <tr>

      <td>Tiny</td>

      <td>1d3</td>

      </tr>

      <tr>

      <td>Small</td>

      <td>1d4</td>

      </tr>

      <tr>

      <td>Medium</td>

      <td>1d6</td>

      </tr>

      <tr>

      <td>Large</td>

      <td>1d8</td>

      </tr>

      <tr>

      <td>Huge</td>

      <td>2d6</td>

      </tr>

      <tr>

      <td>Gargantuan</td>

      <td>2d8</td>

      </tr>

      <tr>

      <td>Colossal</td>

      <td>4d6</td>

      </tr>

      </tbody>

      </table>

      <p><b>Special Attacks:</b> An Osirion mummy retains the base creature's
      special attacks and also gains the following.<p><em>Dust Stroke (Su)
      </em>A successful natural or slam attack by an Osirion mummy that drops
      its victim's hit points to below -9 does more than just kill the victim,
      it also disintegrates the victim's body into a cloud of dust and ash. A
      raise dead spell cannot bring back the victim, but a resurrection still
      works.<p><em>Sudden Burst of Vengeance (Su) </em>Despite its slow
      lumbering nature, an Osirion mummy is capable of lurching forward to
      attack with a short but surprising, explosion of speed. Twice per day, as
      a free action, an Osirion mummy may act as though augmented by a haste
      spell. The effect lasts for a single round.<p><b>Abilities:</b> An Osirion
      mummy's ability scores are modified as follows: Str +4, Int -2 (minimum
      1). As an undead creature, an Osmon mummy has no Constitution
      score.<p><b>Feats:</b> The creature gains Improved Natural Attack for each
      natural attack form as a bonus feat. If the creature previously had a slam
      attack before adding the template. the creature's new slam attack also
      gains the Improved Natural Attack feat.<p><b>Environment:</b>
      Any.<p><b>Challenge Rating:</b> As base creature +1.<p><b>Alignment:</b>
      Usually lawful evil.</p>
  subType: template
type: feat
