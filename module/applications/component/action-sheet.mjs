export class ItemActionSheet extends FormApplication {
  constructor(...args) {
    super(...args);

    this.item.apps[this.appId] = this;
    this.action.apps[this.appId] = this;
  }

  static get defaultOptions() {
    const options = super.defaultOptions;
    return {
      ...options,
      template: "systems/pf1/templates/apps/item-action.hbs",
      classes: [...options.classes, "pf1", "sheet", "action", "item-action"],
      width: 580,
      height: 720,
      closeOnSubmit: false,
      submitOnClose: true,
      submitOnChange: true,
      resizable: true,
      scrollY: [".tab"],
      tabs: [
        {
          navSelector: "nav.tabs[data-group='primary']",
          contentSelector: "section.primary-body",
          initial: "description",
          group: "primary",
        },
      ],
      dragDrop: [
        {
          dragSelector: "li.conditional",
          dropSelector: 'div[data-tab="conditionals"]',
        },
      ],
    };
  }

  get title() {
    return `${this.item.name}: ${this.action.name}`;
  }

  get id() {
    return `action-sheet-${this.action.uuid.replaceAll(".", "-")}`;
  }

  /** @type {ItemAction} */
  get action() {
    return this.object;
  }

  /** @type {ItemPF} */
  get item() {
    return this.action.item;
  }

  /** @type {ActorPF} */
  get actor() {
    return this.item.actor;
  }

  /**
   * @internal
   * @type {Record<string,string>}
   */
  _activeEdits = {};

  /**
   * Which fields to track edits for
   *
   * @internal
   */
  static EDIT_TRACKING = ["description"];

  async getData() {
    const action = this.action;
    const item = this.item;
    const actor = this.actor;

    const editable = this.isEditable;

    const context = {
      config: pf1.config,
      editable,
      cssClass: editable ? "editable" : "locked",
      user: game.user,
      action, // Derived data
      data: action.toObject(true, false), // Source data
      fields: action.schema.fields,
      item,
      actor,
      img: action.img,
      tag: action._source.tag, // True tag
      damageTypes: pf1.registry.damageTypes.toObject(),
      rollData: action.getRollData(),
      choices: {
        measureTemplateTypes: pf1.utils.internal.getTemplateTypes(),
      },
    };

    // Action Details
    context.hasAttack = action.hasAttack;
    context.actionType = action.actionType;
    context.isHealing = context.actionType === "heal";
    context.hasDamage = action.hasDamage;
    context.isCombatManeuver = action.isCombatManeuver;
    context.canCrit = action.hasAttack && action.ability?.critMult > 1;
    // Can have crit and non-crit damage, or simply show them if they've been defined.
    context.hasCritDamage = context.canCrit || action.damage?.critParts?.length > 0;
    context.hasNonCritDamage = context.canCrit || action.damage?.nonCritParts?.length > 0;

    context.isCharged = action.isCharged;
    context.isSelfCharged = action.isSelfCharged;
    const chargedUsePeriods = new Set([...Object.keys(pf1.config.limitedUsePeriods), "charges"]);
    chargedUsePeriods.delete("single"); // Single is special
    context.choices.limitedUsePeriods = { ...pf1.config.limitedUsePeriods };
    if (!item.isPhysical) delete context.choices.limitedUsePeriods.single;
    context.showMaxChargeFormula = chargedUsePeriods.has(action.uses.self.per);
    if (action.hasRange) {
      context.canInputRange = ["ft", "mi", "spec"].includes(action.range.units);
      context.canInputMinRange = ["ft", "mi", "spec"].includes(action.range.minUnits);
    }

    context.canInputDuration = !["", "turn", "inst", "perm", "seeText"].includes(action.duration?.units || "");

    // Action Details
    context.itemName = item.name;
    context.itemEnh = item.system.enh || 0;
    context.isSpell = item.type === "spell";
    context.usesSpellPoints = item.spellbook?.spellPoints.useSystem;
    context.defaultChargeFormula = item.getDefaultChargeFormula();
    context.owned = actor != null;
    context.parentOwned = actor != null;
    context.owner = item.isOwner;
    context.isGM = game.user.isGM;
    context.unchainedActionEconomy = game.settings.get("pf1", "unchainedActionEconomy");
    context.activation = action.activation;
    context.hasActivationType = context.activation.type;
    context.choices.abilityActivationTypes = context.unchainedActionEconomy
      ? pf1.config.abilityActivationTypes_unchained
      : pf1.config.abilityActivationTypes;

    // Activation types that don't benefit from activation cost field being shown
    context.costlessActivation = !context.activation.type || pf1.config.costlessActivation.has(context.activation.type);

    const noDesc = "<p>" + game.i18n.localize("PF1.NoDescription") + "</p>";

    // Add description
    const description = action.description;
    context.descriptionHTML = description
      ? await pf1.utils.enrichHTMLUnrolled(description, {
          secrets: context.owner,
          rollData: context.rollData,
          relativeTo: this.actor,
        })
      : noDesc;

    // Show additional ranged properties
    context.showMaxRangeIncrements = action.range.units === "ft";

    // Prepare attack specific stuff
    if (item.type === "attack") {
      context.isWeaponAttack = item.system.subType === "weapon";
      context.isNaturalAttack = item.system.subType === "natural";
    }

    context.canUseAmmo = context.isNaturalAttack !== true;
    context.usesAmmo = !!action.ammo.type;
    context.inheritedAmmoType = item?.system.ammo?.type;

    if (context.usesAmmo) {
      context.inheritedMisfire = item?.system.ammo?.misfire ?? null;
    }

    if (context.canUseAmmo) {
      context.choices.ammoTypes = {
        none: game.i18n.localize("PF1.None"),
        "": game.i18n.format("PF1.InheritAs", { inherited: pf1.config.ammoTypes[context.inheritedAmmoType] }),
        ...pf1.config.ammoTypes,
      };
      if (!context.inheritedAmmoType) delete context.choices.ammoTypes[""];
    }

    // Add distance units
    context.choices.distanceUnits = foundry.utils.deepClone(pf1.config.distanceUnits);
    delete context.choices.distanceUnits.none; // same as empty selection
    // Set whether to show minimum range input
    context.minRangeAvailable = ["reach", "ft", "mi", "seeText"].includes(action.range.units);

    // Prepare stuff for actions with conditionals
    if (context.data.conditionals) {
      context.conditionals = [...action.conditionals].map((c) => ({ ...c, _id: c.id }));
      const targets = action.getConditionalTargets();
      context.conditionalTargets = targets;
      context.hasConditionalTargets = !foundry.utils.isEmpty(targets);
      for (const conditional of context.conditionals) {
        if (!conditional.modifiers?.size) continue;
        conditional.modifiers = [...conditional.modifiers].map((m) => ({ ...m, _id: m.id }));
        for (const modifier of conditional.modifiers) {
          modifier.targets = targets;
          modifier.targetEntry = targets[modifier.target];
          modifier.subTargets = modifier.targetEntry?.choices;
          modifier.conditionalModifierTypes = action.getConditionalModifierTypes(modifier.target);
          modifier.conditionalCritical = action.getConditionalCritical(modifier.target);

          // Damage type supporting data
          modifier.damage = new pf1.models.action.DamagePartModel({ types: modifier.damageType });
        }
      }
    }

    // Add materials and addons
    context.materialCategories = this._prepareMaterialsAndAddons();

    context.materialAddons =
      this.action.addonMaterial.reduce((obj, v) => {
        obj[v] = true;
        return obj;
      }, {}) ?? {};

    // Can hold (attacks & weapons only and only if they have attack rolls)
    context.canHold = action.hasAttack;
    // Inherited held option's name if any
    context.inheritedHeld =
      context.canHold && ["attack", "weapon"].includes(item.type)
        ? pf1.config.weaponHoldTypes[context.item.system.held]
        : null;

    // Add alignments
    context.alignmentTypes = this._prepareAlignments(this.action.alignments);
    this.alignments = context.alignmentTypes?.values; // Use a deep clone we've already made to track our progress.

    // Ability damage multiplier from held
    const held = context.rollData.action.held || context.rollData.item.held || "1h";
    context.heldAbilityMultiplier = pf1.config.abilityDamageHeldMultipliers[held] ?? 1;

    // Key and Default ability score
    // TODO: Should be allowed for most things eventually that can be class linked and once class key ability can be configured.
    context.choices ??= {};
    context.hasKeyAbility = this.item?.type === "spell";
    const keyAbility = this.item?.spellbook?.ability;
    const keyAbilityLabel = pf1.config.abilities[keyAbility];

    let defaultAbility;
    switch (action.actionType) {
      case "mwak":
      case "msak":
      case "mcman":
        defaultAbility = actor?.system.attributes?.attack?.meleeAbility || "str";
        break;
      case "rwak":
      case "twak":
      case "rsak":
      case "rcman":
        defaultAbility = actor?.system.attributes?.attack?.rangedAbility || "dex";
        break;
    }

    const defaultAbilityChoice = game.i18n.format("PF1.DefaultRef", {
      value: pf1.config.abilities[defaultAbility] || game.i18n.localize("PF1.None"),
    });
    context.choices.ability = context.hasKeyAbility
      ? {
          _key: game.i18n.format(keyAbility ? "PF1.KeyAbilityRef" : "PF1.KeyAbility", { ability: keyAbilityLabel }),
          _default: defaultAbilityChoice,
          ...pf1.config.abilities,
        }
      : {
          _default: defaultAbilityChoice,
          ...pf1.config.abilities,
        };

    // Power attack multiplier if inherited
    context.paMultiplier = action.getPowerAttackMult({ rollData: context.rollData });

    // Prepare attack configuration
    context.extraAttacksTypes = Object.fromEntries([
      ...Object.entries(pf1.config.extraAttacks).map(([key, { label }]) => [key, label]),
    ]);

    context.extraAttacksConfig = { ...pf1.config.extraAttacks[action.extraAttacks?.type] };
    context.extraAttacksConfig.allowCustom = context.extraAttacksConfig.manual || context.extraAttacksConfig.formula;

    if (this.constructor.EDIT_TRACKING?.length)
      context._editorState = pf1.applications.utils.restoreEditState(this, context.data);

    return context;
  }

  _prepareMaterialsAndAddons() {
    const materialList = {};
    const addonList = [];
    pf1.utils.naturalSort([...pf1.registry.materials], "name").forEach((material) => {
      if (
        material.allowed.lightBlade ||
        material.allowed.oneHandBlade ||
        material.allowed.twoHandBlade ||
        material.allowed.rangedWeapon
      ) {
        if (!material.addon && !material.basic) {
          materialList[material.id] = material.name;
        } else if (material.addon && material.isValidAddon(this.action.normalMaterial)) {
          addonList.push({ key: material.id, name: material.name });
        }
      }
    });

    return {
      materials: materialList,
      addons: addonList,
    };
  }

  _prepareAlignments(alignments = {}) {
    const alignmentChoices = {};

    Object.keys(pf1.config.damageResistances).forEach((dType) => {
      if (!["magic", "epic"].includes(dType)) alignmentChoices[dType] = pf1.config.damageResistances[dType];
    });

    return {
      choices: alignmentChoices,
      values: alignments,
    };
  }

  /**
   * Copy from core DocumentSheet#isEditable
   */
  get isEditable() {
    const parentItem = this.item;
    let editable = this.options.editable && parentItem.isOwner;
    if (parentItem.pack) {
      const pack = game.packs.get(parentItem.pack);
      if (pack.locked) editable = false;
    }
    return editable;
  }

  /**
   * @override
   * @param {JQuery<HTMLElement>} html
   */
  activateListeners(html) {
    super.activateListeners(html);

    if (!this.isEditable) return;

    pf1.applications.utils.trackStaleEditors(this, html[0]);

    // Modify action image
    html.find("img[data-edit]").click((ev) => this._onEditImage(ev));

    // Add drop handler to textareas
    html.find("textarea, .card-notes input[type='text']").on("drop", this._onTextAreaDrop.bind(this));

    // Modify attack formula
    html.find(".attack-control").click(this._onAttackControl.bind(this));

    // Modify damage formula
    html.find(".damage-control").click(this._onDamageControl.bind(this));
    html.find(".damage-type-visual").on("click", this._onClickDamageType.bind(this));

    // Note control
    html.find(".card-notes .controls a").click(this._onNoteEntryControl.bind(this));

    // Modify conditionals
    html.find(".conditional-control").click(this._onConditionalControl.bind(this));

    // Handle Alignment tristate checkboxes
    html.find(".alignmentCheckbox").on("change", (event) => {
      this._onAlignmentChecked(event);
    });

    html.find(".alignmentCheckbox.actionCheckbox").each((index, item) => {
      const type = item.dataset.type;
      if (!(this.alignments[type] || this.alignments[type] === false)) {
        item.indeterminate = true;
      }
    });
  }

  _onDragStart(event) {
    const elem = event.currentTarget;

    console.log(elem);

    // Drag conditional
    if (elem.dataset?.conditional) {
      const conditional = this.action.conditionals.get(elem.dataset?.conditional);
      const dragData = {
        type: "pf1Conditional",
        data: conditional.toObject(),
      };
      event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }
  }

  /**
   * Foundry defaults to isGM check.
   *
   * @override
   */
  _canDragStart(selector) {
    return this.isEditable;
  }

  /**
   * Foundry defaults to isGM check.
   *
   * @override
   */
  _canDragDrop(selector) {
    return this.isEditable;
  }

  async _onDrop(event) {
    event.preventDefault();
    event.stopPropagation();

    const dropData = TextEditor.getDragEventData(event);
    const { type } = dropData;

    const action = this.action;
    switch (type) {
      // Handle conditionals
      case "pf1Conditional": {
        const conditional = dropData.data;
        // Check targets and other fields for valid values, reset if necessary
        for (const modifier of conditional.modifiers) {
          if (!Object.keys(action.getConditionalTargets()).includes(modifier.target)) {
            delete modifier.target;
          }
          if (modifier.subTarget) {
            const results = action.getConditionalTargets();
            const choices = results?.[modifier.target]?.choices;
            if (!choices) delete modifier.subTarget;
            else if (!Object.keys(choices).includes(modifier.subTarget)) delete modifier.subTarget;
          }
          if (modifier.type) {
            const keys = Object.keys(action.getConditionalModifierTypes(modifier.target));
            if (!keys.includes(modifier.type)) delete modifier.type;
          }
          if (modifier.critical) {
            const keys = Object.keys(action.getConditionalCritical(modifier.target));
            if (!keys.includes(modifier.critical)) delete modifier.critical;
          }
        }

        // Renew conditional ID
        delete conditional._id;

        await pf1.components.ItemConditional.create(conditional, { parent: this.action });
      }
    }
  }

  async _onNoteEntryControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const key = a.dataset.name;

    switch (a.dataset.action) {
      // Add
      case "add": {
        const notes = foundry.utils.getProperty(this.action.toObject(), key) ?? [];
        notes.push("");
        const updateData = { [key]: notes };
        return this._updateObject(event, this._getSubmitData(updateData));
      }
      // Delete
      case "delete": {
        const index = Number(a.dataset.index);
        const notes = foundry.utils.getProperty(this.action.toObject(), key) ?? [];
        notes.splice(index, 1);
        const updateData = { [key]: notes };
        return this._updateObject(event, this._getSubmitData(updateData));
      }
    }
  }

  async _onConditionalControl(event) {
    event.preventDefault();
    const a = event.currentTarget;

    await this._onSubmit(event, { preventRender: true }); // Submit any unsaved changes

    // Add new conditional
    if (a.classList.contains("add-conditional")) {
      return pf1.components.ItemConditional.create([{}], { parent: this.action });
    }
    // Remove a conditional
    else if (a.classList.contains("delete-conditional")) {
      const li = a.closest(".conditional");
      const conditional = this.action.conditionals.get(li.dataset.conditional);
      return conditional.delete();
    }
    // Add a new conditional modifier
    else if (a.classList.contains("add-conditional-modifier")) {
      const li = a.closest(".conditional");
      const conditional = this.action.conditionals.get(li.dataset.conditional);
      return pf1.components.ItemConditionalModifier.create([{}], { parent: conditional });
    }
    // Remove a conditional modifier
    else if (a.classList.contains("delete-conditional-modifier")) {
      const li = a.closest(".conditional-modifier");
      const conditional = this.action.conditionals.get(li.dataset.conditional);
      const modifier = conditional.modifiers.get(li.dataset.modifier);
      return modifier.delete();
    }
  }

  /**
   * Add or remove a damage part from the damage formula
   *
   * @param {Event} event     The original click event
   * @returns {Promise}
   * @private
   */
  async _onDamageControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const list = a.closest(".damage");
    const path = list.dataset.key || "damage.parts";
    const k2 = path.split(".").slice(0, -1).join(".");
    const k3 = path.split(".").slice(-1).join(".");

    // Add new damage component
    if (a.classList.contains("add-damage")) {
      // Add data
      const damage = foundry.utils.getProperty(this.action.toObject(), k2);
      const damageParts = foundry.utils.getProperty(damage, k3) ?? [];
      damageParts.push({});
      const updateData = { [path]: damageParts };
      return this._updateObject(event, this._getSubmitData(updateData));
    }
    // Remove a damage component
    else if (a.classList.contains("delete-damage")) {
      const li = a.closest(".damage-part");
      const damage = foundry.utils.getProperty(this.action.toObject(), k2);
      const damageParts = foundry.utils.getProperty(damage, k3) ?? [];
      if (damageParts.length) {
        damageParts.splice(Number(li.dataset.damagePart), 1);
        const updateData = { [path]: damageParts };
        return this._updateObject(event, this._getSubmitData(updateData));
      }
    }
  }

  /**
   * @internal
   * @param {Event} event
   */
  async _onClickDamageType(event) {
    event.preventDefault();
    const elem = event.currentTarget;

    // Check for normal damage part
    const damageIndex = Number(elem.closest(".damage-part")?.dataset.damagePart);
    const damagePart = elem.closest(".damage")?.dataset.key;

    if (damageIndex >= 0 && damagePart) {
      const app = new pf1.applications.DamageTypeSelector(
        this.action,
        `${damagePart}.${damageIndex}.types`,
        foundry.utils.getProperty(this.action, damagePart)[damageIndex].types,
        {
          updateCallback: (update) => {
            const damageArray = foundry.utils.getProperty(this.action.toObject(), damagePart) ?? [];
            damageArray[damageIndex].types = update;
            this.action.update({ [damagePart]: damageArray });
          },
        }
      );
      return app.render(true);
    }

    // Check for conditional
    const conditionalEl = elem.closest(".conditional");
    const modifierEl = elem.closest(".conditional-modifier");

    /** @type {ItemConditional} */
    const conditional = this.action.conditionals.get(conditionalEl?.dataset.conditional);

    /** @type {ItemConditionalModifier} */
    const modifier = conditional?.modifiers.get(modifierEl?.dataset.modifier);

    if (!modifier) return void console.warn("Conditional modifier not found!");

    const app = new pf1.applications.DamageTypeSelector(modifier, "damageType", modifier.damageType, {
      updateCallback: (update) => {
        modifier.update({ damageType: update });
      },
    });
    return app.render(true);
  }

  async _onAttackControl(event) {
    event.preventDefault();
    const a = event.currentTarget;

    const manualExtraAttacks = this.action.toObject().extraAttacks?.manual ?? [];

    // Add new attack component
    if (a.classList.contains("add-attack")) {
      manualExtraAttacks.push({ formula: "", name: "" });
      const updateData = { extraAttacks: { manual: manualExtraAttacks } };
      return this._updateObject(event, this._getSubmitData(updateData));
    }
    // Remove an attack component
    else if (a.classList.contains("delete-attack")) {
      const li = a.closest(".attack-part");
      manualExtraAttacks.splice(Number(li.dataset.attackPart), 1);
      const updateData = { extraAttacks: { manual: manualExtraAttacks } };
      return this._updateObject(event, this._getSubmitData(updateData));
    }
  }

  /**
   * Clone of item/actor sheet image editor callback.
   *
   * @protected
   * @param {Event} event
   */
  _onEditImage(event) {
    const attr = event.currentTarget.dataset.edit;
    const current = foundry.utils.getProperty(this.action, attr);
    const fp = new FilePicker({
      type: "image",
      current,
      callback: (path) => this.action.update({ img: path }),
      top: this.position.top + 40,
      left: this.position.left + 10,
    });
    fp.browse();
  }

  async _onTextAreaDrop(event) {
    event.preventDefault();

    const eventData = TextEditor.getDragEventData(event.originalEvent);
    if (!eventData) return;

    const elem = event.currentTarget;
    const link = await TextEditor.getContentLink(eventData, { relativeTo: this.actor });

    // Insert link
    if (link) {
      elem.value = !elem.value ? link : elem.value + "\n" + link;

      return this._onSubmit(event); // Save
    }
  }

  async _onAlignmentChecked(event) {
    const el = event.target;
    const previousValue = this.alignments[el.dataset.type];

    // Move from one phase to the next in our tristate checkbox
    el.checked = previousValue === false;
    el.indeterminate = previousValue === true;

    if (el.checked) {
      this.alignments[el.dataset.type] = true;
    } else if (el.indeterminate) {
      this.alignments[el.dataset.type] = null;
    } else {
      this.alignments[el.dataset.type] = false;
    }
  }

  async _updateObject(event, formData) {
    const oldData = this.action.toObject(true, false);

    formData = foundry.utils.expandObject(formData);

    // Handle conditionals array, merging incomplete data with old data
    if (formData.conditionals) {
      // Preserve arrays
      // Convert {0:{}} to [{}] keeping order of keys
      const preserveArray = (obj) =>
        Object.entries(obj ?? {})
          .sort(([k0], [k1]) => k0 - k1)
          .map(([_, data]) => data);

      // Convert to arrays and merge old data
      // mergeObject() does not do deep merging into arrays, so this is somewhat manual
      const oldConds = oldData.conditionals ?? [];
      const conditionals = preserveArray(formData.conditionals);
      for (let ci = 0; ci < conditionals.length; ci++) {
        const c = conditionals[ci];
        c.modifiers = preserveArray(c.modifiers);

        const oldC = oldConds[ci];
        for (let mi = 0; mi < c.modifiers.length; mi++) {
          const m = c.modifiers[mi];
          const oldM = oldC.modifiers[mi];

          c.modifiers[mi] = foundry.utils.mergeObject(oldM, m);
        }
        conditionals[ci] = foundry.utils.mergeObject(oldC, c);
      }

      formData.conditionals = conditionals;
    }

    // Merge partial damage data to preserve overall data
    if (formData.damage) {
      for (const [key, array] of Object.entries(formData.damage)) {
        if (Array.isArray(array)) continue;
        const damage = oldData.damage[key] ?? [];
        for (const [idx, data] of Object.entries(array)) {
          damage[idx] = foundry.utils.mergeObject(damage[Number(idx)] ?? {}, data);
        }
        formData.damage[key] = damage;
      }
    }

    // Adjust Material Addons object to array
    const material = formData.material;
    if (material?.addon) {
      material.addon = Object.entries(material.addon)
        .filter(([_, chosen]) => chosen)
        .map(([key]) => key);
    }

    if (formData.alignments) {
      // Adjust Alignment Types (this is necessary to handle null values for inheritance)
      for (const [key, value] of Object.entries(this.alignments)) {
        formData.alignments[key] = value;
      }
    }

    // Do not add image if it's same as parent item
    if (formData.img === this.item?.img) delete formData.img;

    return this.action.update(formData);
  }

  async close(options = {}) {
    delete this.item.apps[this.appId];
    delete this.action.apps[this.appId];
    if (this.action._sheet === this) this.action._sheet = null;

    if (options.force && this._state <= Application.RENDER_STATES.NONE) return; // HACK: already closed, would error without

    return super.close(options);
  }

  /**
   * Copy of DocumentSheet._createDocumentIdLink
   *
   * @internal
   * @param {jQuery} html
   * Synced with Foundry v12.331
   */
  _createDocumentIdLink(html) {
    if (!this.object.id) return;
    const title = html.find(".window-title");
    const label = game.i18n.localize(this.object.constructor.metadata.label);
    const idLink = document.createElement("a");
    idLink.classList.add("document-id-link");
    idLink.ariaLabel = game.i18n.localize("SHEETS.CopyUuid");
    idLink.dataset.tooltip = `SHEETS.CopyUuid`;
    idLink.dataset.tooltipDirection = "UP";
    idLink.innerHTML = '<i class="fa-solid fa-passport"></i>';
    idLink.addEventListener("click", (event) => {
      event.preventDefault();
      game.clipboard.copyPlainText(this.object.uuid);
      ui.notifications.info(
        game.i18n.format("DOCUMENT.IdCopiedClipboard", { label, type: "uuid", id: this.object.uuid })
      );
    });
    idLink.addEventListener("contextmenu", (event) => {
      event.preventDefault();
      game.clipboard.copyPlainText(this.object.id);
      ui.notifications.info(game.i18n.format("DOCUMENT.IdCopiedClipboard", { label, type: "id", id: this.object.id }));
    });
    title.append(idLink);
  }

  /**
   * Used to call {@link _createDocumentIdLink}
   *
   * @override
   */
  async _renderOuter() {
    const html = await super._renderOuter();
    this._createDocumentIdLink(html);
    return html;
  }

  async _render(force, options = {}) {
    pf1.applications.utils.saveEditState(this, options);

    return super._render(force, options);
  }
}
