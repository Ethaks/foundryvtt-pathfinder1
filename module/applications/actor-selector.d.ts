interface ActorSelectorOptions {
  /**
   * Actor list to choose from
   */
  actors: Actor[];
  /**
   * Filtering callback function
   */
  filterFunc: Function<Actor>;
  /**
   * Pre-selected actor ID.
   */
  selected: string;
  /**
   * Minimum Ownership level.
   *
   * @defaultValue `CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED`
   */
  ownership: string | number;
}
