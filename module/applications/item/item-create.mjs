import { NoAutocomplete } from "@app/mixins/no-autocomplete.mjs";

const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

/**
 * Derivate of Foundry's Item.createDialog() functionality.
 */
export class ItemCreateDialog extends HandlebarsApplicationMixin(NoAutocomplete(ApplicationV2)) {
  /**
   * @param {object} [options.data] - Initial item data
   * @param {object} [options] - Optional configuration
   * @param {Function} [options.resolve] - Resolve callback
   * @param {*} [options.pack=null] - Pack reference given to Item.create()
   * @param {*} [options.parent=null] - Parent reference given to Item.create()
   * @param {object} [options.options] - FormApplication options
   * @param {Array<string>} options.types - Array of types to limit the choices to.
   */
  constructor({ data = {}, resolve, ...options } = {}) {
    if (options.types) options.types = new Set(options.types);

    super(options);

    this.resolve = resolve;
    this.object = data;

    this._updateCreationData(data);
  }

  /** @override */
  get title() {
    return game.i18n.format("DOCUMENT.Create", { type: game.i18n.localize("DOCUMENT.Item") });
  }

  getDefaultName(form = true) {
    const formData =
      form && this.form ? foundry.utils.expandObject(new FormDataExtended(this.form).object) : this.createData;

    const type = formData.type;
    const subType = formData.system?.subType;

    return Item.implementation.defaultName({ type, subType, pack: this.options.pack, parent: this.options.parent });
  }

  static DEFAULT_OPTIONS = {
    tag: "form",
    form: {
      handler: this._onSubmit,
    },
    position: {
      height: "auto",
    },
    classes: ["pf1-v2", "create-document", "create-item"],
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/widgets/item-create.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  get initialData() {
    return this.object;
  }

  /** @type {object} */
  createData = {};

  /**
   *
   * @param {string} type - Item type
   * @returns {Record<string,string>|null} - Option mapping
   */
  getSubtypes(type) {
    switch (type) {
      case "class":
        return pf1.config.classTypes;
      case "race":
        return null;
      case "attack":
        return pf1.config.attackTypes;
      case "feat":
        return pf1.config.featTypes;
      case "weapon":
        return Object.entries(pf1.config.weaponTypes).reduce((all, [key, value]) => {
          all[key] = value._label;
          return all;
        }, {});
      case "equipment":
        return Object.entries(pf1.config.equipmentTypes).reduce((all, [key, value]) => {
          all[key] = value._label;
          return all;
        }, {});
      case "consumable":
        return pf1.config.consumableTypes;
      case "loot":
        return pf1.config.lootTypes;
      case "spell":
        return null;
      case "buff":
        return pf1.config.buffTypes;
      case "implant":
        return pf1.config.implantTypes;
      case "container":
        return null;
      default:
        return null;
    }
  }

  /**
   * Called by {@link _preparePartContext}
   *
   * @internal
   * @param {object} context - Context
   * @param {object} options - Options
   * @returns {object} - Same as context param
   */
  _prepareFormContext(context, options) {
    const lang = game.i18n.lang;

    let collection;
    if (!this.options.parent) {
      if (this.options.pack) collection = game.packs.get(this.options.pack);
      else collection = game.items;
    }

    // TODO: Visualize folder tree better
    const folders = Object.fromEntries(
      (collection?._formatFolderSelectOptions() ?? []).map(({ id, name }) => [id, name])
    );

    const createData = this.createData;

    let subtypes = this.getSubtypes(createData.type);
    if (!subtypes && createData.system?.subType !== undefined) delete createData.system.subType;
    if (subtypes) {
      subtypes = Object.fromEntries(
        Object.entries(subtypes).sort(([key0, label0], [key1, label1]) => label0.localeCompare(label1, lang))
      );

      if (!subtypes[this.createData.system.subType]) {
        this.createData.system.subType = Object.keys(subtypes)[0];
      }
    } else {
      this.createData.system.subType = undefined;
    }

    const types = Object.fromEntries(
      Object.entries(CONFIG.Item.typeLabels).sort(([key0, label0], [key1, label1]) =>
        label0.localeCompare(label1, lang)
      )
    );
    delete types.base; // base is Foundry's unusable default
    if (this.options.types) {
      for (const type of Object.keys(types)) {
        if (!this.options.types.has(type)) delete types[type];
      }
    }

    context.name = createData.name;
    context.defaultName = this.getDefaultName(false);
    context.folder = createData.folder;
    context.type = createData.type;
    context.subtype = createData.system?.subType || null;
    context.choices = {
      folders,
      types,
      subtypes,
    };

    if (Object.keys(folders).length == 0) delete context.choices.folders;

    return context;
  }

  /**
   * @override
   * @param {string} partId
   * @param {object} context
   * @param {object} options
   * @returns {object}
   */
  _preparePartContext(partId, context, options) {
    switch (partId) {
      case "footer": {
        // Footer buttons
        context.buttons = [
          {
            action: "create",
            type: "submit",
            icon: "fa-solid fa-check",
            label: game.i18n.format("DOCUMENT.Create", { type: game.i18n.localize("DOCUMENT.Item") }),
          },
        ];
        break;
      }
      case "form": {
        return this._prepareFormContext(context, options);
      }
    }

    return context;
  }

  /**
   * @override
   * @param {object} formConfig
   * @param {Event} event
   */
  _onChangeForm(formConfig, event) {
    this._updateCreationData(new FormDataExtended(event.currentTarget).object);

    super._onChangeForm(formConfig, event);

    // HACK: Don't re-render on input changes, we have no reactive elements for them.
    if (event.target.tagName === "INPUT") return;

    this.render({ parts: ["form"] });
  }

  /**
   * @internal
   * @param {object} data
   */
  _updateCreationData(data = {}) {
    // Fill in default type if missing
    data.type ||= CONFIG.Item.defaultType || game.documentTypes.Item[1];
    // If type does not match restrictions, assume first type is correct
    if (this.options.types && !this.options.types.has(data.type)) data.type = this.options.types.first();

    this.createData = foundry.utils.mergeObject(this.initialData, data, { inplace: false });
    this.createData.system ??= {};

    // Clean up data
    if (!data.folder && !this.initialData.folder) delete this.createData.folder;

    const subtypes = this.getSubtypes(this.createData.type);
    if (!subtypes) delete this.createData.system.subType;

    return this.createData;
  }

  /**
   * @override
   * @param {SubmitEvent} event
   * @param {HTMLFormElement} form
   * @param {FormDataExtended} formData
   */
  static async _onSubmit(event, form, formData) {
    const createData = this._updateCreationData(formData.object);
    createData.name ||= this.getDefaultName(false);

    const options = {};
    if (this.options.pack) options.pack = this.options.pack;
    if (this.options.parent) options.parent = this.options.parent;
    options.renderSheet = true;

    const promise = Item.implementation.create(createData, options);

    this.resolve?.(promise);

    this.close();
  }

  /** @override */
  _onClose(options) {
    super._onClose(options);
    this.resolve?.(null);
  }

  /**
   * Wait for dialog to the resolved.
   *
   * @param {object} [data] Initial data to pass to the constructor.
   * @param {object} [options] Options to pass to the constructor.
   * @returns {Promise<Item|null>} Created item or null.
   */
  static waitPrompt(options = {}) {
    return new Promise((resolve) => {
      options.resolve = resolve;
      new this(options).render(true);
    });
  }
}
