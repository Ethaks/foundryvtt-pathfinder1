/**
 * Trap actor data model
 */
export class TrapModel extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      bypass: new fields.StringField({
        blank: true,
        choices: () => pf1.config.traps.bypass,
      }),
      cr: new fields.SchemaField({
        base: new fields.NumberField({
          initial: 0,
          min: 0,
          nullable: false,
        }),
      }),
      disarm: new fields.NumberField({
        initial: 0,
        integer: true,
        min: 0,
        nullable: false,
      }),
      duration: new fields.SchemaField({
        value: new fields.StringField(),
        units: new fields.StringField({
          blank: true,
          initial: "inst",
          choices: () => pf1.config.timePeriods,
        }),
      }),
      effect: new fields.HTMLField(),
      notes: new fields.HTMLField(),
      perception: new fields.NumberField({
        initial: 0,
        integer: true,
        min: 0,
        nullable: false,
      }),
      reset: new fields.SchemaField({
        type: new fields.StringField({
          blank: true,
          choices: () => pf1.config.traps.reset,
        }),
        value: new fields.StringField(),
        units: new fields.StringField({
          blank: true,
          initial: "inst",
          choices: () => pf1.config.timePeriods,
        }),
      }),
      trigger: new fields.SchemaField({
        type: new fields.StringField({
          blank: true,
          choices: () => pf1.config.traps.trigger,
        }),
        value: new fields.StringField(),
        vision: new fields.StringField({
          blank: true,
          choices: () => pf1.config.traps.vision,
        }),
        units: new fields.StringField({
          blank: true,
          initial: "inst",
          choices: () => pf1.config.timePeriods,
        }),
      }),
      type: new fields.StringField({
        initial: "mechanical",
      }),
      technology: new fields.BooleanField({
        initial: false,
      }),
    };
  }

  static migrateData(source) {
    if (!source.details) return;

    // Migrate CR
    if (source.details.cr) {
      source.cr ??= {
        base: source.details.cr.base,
      };
    }

    // Migrate flat fields
    ["disarm", "effect", "perception"].forEach((key) => {
      if (source.details[key]) {
        source[key] ??= source.details[key];
      }
    });

    // Make sure the type is lowercase (actor breaks if type was "Magic" instead of "magic", for example)
    if (source.details.type) {
      const old = source.details.type;
      for (const [key, value] of Object.entries(pf1.config.traps.types)) {
        if (value === old) {
          source.type ??= key;
          break;
        }
      }
      source.type ??= old;
    }

    // Migrate Duration
    if (source.details.duration) {
      source.duration ??= {
        value: source.details.duration,
        units: "",
      };
    }

    // Migrate Reset
    if (source.details.reset) {
      source.reset ??= {
        type: "automatic",
        value: source.details.reset,
        units: "",
      };
    }

    // Migrate Trigger
    if (source.details.trigger) {
      source.duration ??= {
        type: "location",
        value: source.details.trigger,
        units: "",
      };
    }

    // Migrate Notes
    if (typeof source.details?.notes?.value === "string") {
      source.notes = source.details.notes.value;
    }

    // Clean up the Details
    delete source.details;
  }
}
