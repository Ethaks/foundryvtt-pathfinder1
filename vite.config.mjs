import path from "node:path/posix";

import { defineConfig } from "vite";
import { visualizer } from "rollup-plugin-visualizer";
import { copy } from "@guanghechen/rollup-plugin-copy";
import tsconfigPaths from "vite-tsconfig-paths";

import { resolveUrl, FOUNDRY_CONFIG } from "./tools/foundry-config.mjs";
import handlebarsReload from "./tools/handlebars-reload.mjs";
import langReload from "./tools/lang-reload.mjs";
import forceMinifyEsm from "./tools/minify.mjs";
import syncStaticFiles from "./tools/sync-static-files.mjs";

const COPY_FILES = ["CREDITS.md", "LICENSE.txt", "CHANGELOG.md", "OGL.txt", "help"];

const config = defineConfig(({ command, mode }) => {
  const port = FOUNDRY_CONFIG.port;

  return {
    root: ".",
    base: resolveUrl("systems/pf1/"),
    publicDir: "public",
    server: {
      port: 30001,
      open: FOUNDRY_CONFIG.openBrowser,
      proxy: {
        [`^(?!${resolveUrl("systems/pf1")})`]: `http://localhost:${port}/`,
        [resolveUrl("socket.io/")]: {
          target: `ws://localhost:${port}`,
          ws: true,
        },
      },
    },
    build: {
      minify: false,
      target: "es2022",
      outDir: "dist",
      emptyOutDir: true,
      sourcemap: true,
      rollupOptions: {
        external: /^(..\/..\/)?(fonts|ui|icons)\//,
        output: {
          sourcemapPathTransform: (relative) => {
            // Relative paths start with a `../`, which moves the path out of the `systems/pf1` directory.
            if (relative.startsWith("../")) relative = relative.replace("../", "");
            return relative;
          },
        },
      },
      reportCompressedSize: true,
      lib: {
        name: "pf1",
        entry: "pf1.mjs",
        formats: ["es"],
        fileName: () => "pf1.js",
        cssFileName: "pf1",
      },
    },
    css: {
      devSourcemap: true,
      preprocessorOptions: {
        // the usual urls in less will work within Foundry due to file placement,
        // but the dev server would resolve them from the root instead of relative to the file
        less: {
          rootpath: command === "serve" ? "systems/pf1/" : "",
          rewriteUrls: "off", // causes CSS paths to be malformed if set to anything else
        },
      },
    },
    plugins: [
      tsconfigPaths({
        root: path.resolve("."),
        projects: ["jsconfig.json"],
      }),
      forceMinifyEsm(),
      visualizer({
        sourcemap: true,
        template: "treemap",
      }),
      copy({ targets: [{ src: COPY_FILES, dest: path.resolve("dist") }], hook: "writeBundle" }),
      handlebarsReload(),
      langReload(),
      syncStaticFiles(),
    ],
  };
});

export default config;
